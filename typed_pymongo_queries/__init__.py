# See the mypy/plugin.py of your current python and mypy version for documentation

import sys
import typing as t

from mypy.nodes import AssignmentStmt, DictExpr, MemberExpr, NameExpr, StrExpr, Var
from mypy.options import Options
from mypy.plugin import (
    AnalyzeTypeContext,
    AttributeContext,
    ClassDefContext,
    FunctionContext,
    MethodContext,
    Plugin,
)
from mypy.types import CallableType, Instance, Type, TypeType


class PymongoQueriesPlugin(Plugin):
    def __init__(self, options: Options) -> None:
        super().__init__(options)
        self._method_hooks: t.Dict[str, t.Callable[[MethodContext], Type]] = {}

    def get_base_class_hook(
        self, fullname: str
    ) -> t.Optional[t.Callable[[ClassDefContext], None]]:
        if fullname == "umongo.document.Document":
            return self._build_method_hook
        return None

    def get_method_hook(
        self, fullname: str
    ) -> t.Optional[t.Callable[[MethodContext], Type]]:
        try:
            class_name, _ = fullname.rsplit(".", maxsplit=1)
        except ValueError:
            return None
        return self._method_hooks.get(class_name, None)

    def _build_method_hook(self, ctx: ClassDefContext) -> None:
        cls = ctx.cls
        cls_name = cls.fullname
        fields = {
            value.name
            for stmt in cls.defs.body
            if isinstance(stmt, AssignmentStmt)
            for value in stmt.lvalues
            if isinstance(value, NameExpr)
        }

        def check_field_names(ctx: MethodContext) -> Type:
            [[filtre], _, _] = ctx.args
            if isinstance(filtre, DictExpr):
                for key, _value in filtre.items:
                    if isinstance(key, StrExpr) and key.value not in fields:
                        ctx.api.msg.fail(
                            f'{cls_name} has no field "{key.value}"', context=key
                        )
            return ctx.default_return_type

        self._method_hooks[cls_name] = check_field_names

    def get_function_hook(
        self, fullname: str
    ) -> t.Optional[t.Callable[[FunctionContext], Type]]:
        try:
            # E.g.: "register of PyMongoInstance"
            method_name, _class_name = fullname.split(" of ", maxsplit=1)
        except ValueError:
            return None
        if method_name == "register":
            return self._inject_document_implementation_into_mro
        return None

    def _inject_document_implementation_into_mro(self, ctx: FunctionContext) -> Type:
        # Mypy treats class decorators differently: The decorated class remains unchanged.
        # Additionally, the @instance.register decorator doesn't trigger the
        # get_class_decorator_hook (I don't know why) so I'm basically working
        # around two issues here.

        [[cls_constructor]] = ctx.arg_types
        if (
            isinstance(cls_constructor, CallableType)
            and isinstance(cls_constructor.ret_type, Instance)
            and isinstance(ctx.default_return_type, TypeType)
            and isinstance(ctx.default_return_type.item, Instance)
        ):
            # cls_constructor is e.g.: def (*args: Any, **kwargs: Any) -> main.MyDocument
            # ctx.default_return_type is e.g.: Type[umongo.frameworks.pymongo.PyMongoDocument*]

            # Inject all the superclasses of the DocumentImplementation into the
            # superclasses of the decorated class (after itself, to allow overrides)
            cls_constructor.ret_type.type.mro[
                1:1
            ] = ctx.default_return_type.item.type.mro
        return ctx.default_return_type


def plugin(version: str) -> t.Type[Plugin]:
    """Plugin's public API and entrypoint."""
    return PymongoQueriesPlugin
