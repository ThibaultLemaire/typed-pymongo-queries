# Typed Pymongo Queries

## Getting started

```sh
nix-shell
```

## Testing

```sh
pytest
```

## Formatting code

```sh
isort .
black .
```
