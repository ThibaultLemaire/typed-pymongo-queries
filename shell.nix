{ pkgs ? import <nixpkgs> { } }:

with pkgs;
let
  mypy = with python38Packages;
    buildPythonPackage rec {
      pname = "mypy";
      version = "0.910";

      src = fetchPypi {
        inherit pname version;
        sha256 = "0l31s5pr3j4xkqg44ac2q6s30srp7f3mlxzi32i33jvk4hq9hh3h";
      };

      propagatedBuildInputs = [ toml mypy-extensions typing-extensions ];

      # No tests
      doCheck = false;
    };
  pytest-mypy-plugins = with python38Packages;
    buildPythonPackage rec {
      pname = "pytest-mypy-plugins";
      version = "1.6.1";

      src = fetchPypi {
        inherit pname version;
        sha256 = "1ipwsz1p7v4nd7mivr4cd6db31b9i4cv10cba5cz4mw0sza8cfgp";
      };

      propagatedBuildInputs = [ pyyaml mypy decorator pystache pytest ];

      # No tests
      doCheck = false;
    };
  pymongo-stubs = with python38Packages;
    buildPythonPackage rec {
      pname = "pymongo-stubs";
      version = "0.1.0";

      src = fetchPypi {
        inherit pname version;
        sha256 = "1xzr01zcw2nfnxlxq5cfbd29mvnaba286gnd3zyjlgk8733zvksi";
      };

      propagatedBuildInputs = [ pymongo ];

      # No tests
      doCheck = false;
    };
  umongo = with python38Packages;
    buildPythonPackage rec {
      pname = "umongo";
      version = "3.0.0+typed";

      src = fetchFromGitHub {
         owner = "ThibaultLemaire";
         repo = "umongo";
         rev = "bd3f3a1e477aa6effcec23a5169ace5bec5be4c4";
         sha256 = "165vcxd8514pkh42k8x2sz4mkr4j11v17lng2k6pk7rrcrw9mkyr";
       };

      propagatedBuildInputs = [ pymongo marshmallow ];

      # No tests
      doCheck = false;
    };
  customPython = python38.withPackages (ps:
    with ps; [
      mypy
      pytest
      pytest-mypy-plugins
      pymongo-stubs

      umongo

      black
      isort
    ]);
in customPython.env
